package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import backend.FileHelp;
import backend.Habilidad;
import backend.Juego;
import backend.Personaje;
import backend.Posicion;
import backend.Tablero;
import backend.TipoHabilidad;

public class MainClass {

	
	public static void main (String args[]) {
		
		ArrayList<Habilidad> habilidadesP1 = new ArrayList<Habilidad>();
		habilidadesP1.add(new Habilidad(TipoHabilidad.AtacarLejos));
		habilidadesP1.add(new Habilidad(TipoHabilidad.Atacar));
		
		ArrayList<Habilidad> habilidadesP2 = new ArrayList<Habilidad>();
		habilidadesP2.add(new Habilidad(TipoHabilidad.AtacarLejos));
		habilidadesP2.add(new Habilidad(TipoHabilidad.Atacar));
		
		Personaje p1 = new Personaje(habilidadesP1, "pepe", 1, 10);
		Personaje p2 = new Personaje(habilidadesP2, "carlos", 20, 10);
		p2.getHabilidades().get(0).setLevel(2);
		
		ArrayList<Personaje> personajes = new ArrayList<Personaje>();
		personajes.add(p1);
		personajes.add(p2);
		
		Map<String, Posicion> posiciones = new HashMap<String, Posicion>();
		posiciones.put(p1.getNombre(), new Posicion(2, 2));
		posiciones.put(p2.getNombre(), new Posicion(3, 2));
		
		Juego juego = new Juego(personajes, posiciones, new Tablero(10, 10));
		
		boolean jaqueMate = juego.jaqueMate(p1);
		System.out.println(p1.getNombre() + " esta en Jaque Mate = " + jaqueMate);
		
		////Prueba victimasporcercania/////
		ArrayList<Personaje> VIC=juego.victimasPorCercania(p1);;
		Iterator<Personaje> I=VIC.iterator();
		while(I.hasNext()){
			Personaje P=I.next();
			System.out.println(P.getNombre()+" ");
		}	
		//////////////////////////////////
		
		Personaje elVengador = juego.elVengador();
		System.out.println("El mas vengador es: " + elVengador.getNombre());
		
		ArrayList<Personaje> losMasPoderosos = juego.losMasPoderosos();
		System.out.println("El mas poderoso es: " + losMasPoderosos.get(0).getNombre());
		
		juego.atacar(p1);
		
		System.out.print(" ");
		
		try {
//			FileHelp.Write("archivoPrueba.txt", juego);
			@SuppressWarnings("unused")
			Juego leerJuego = FileHelp.Read("archivoPrueba.txt", Juego.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
