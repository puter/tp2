package backend;

import java.io.IOException;
import java.io.Writer;
import java.util.StringTokenizer;

public class Posicion 
{
	
	private int x;
	private int y;
    
	
	public Posicion(int ax, int ay){
		setX(ax);
		setY(ay);
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}


    public static void escribir(Writer out, Posicion t) throws IOException{
    	out.write("( "+t.getX()+" , "+t.getY()+" )");
	}
	
	public static Posicion leer(StringTokenizer in) throws IOException{
		in.nextToken();//Dejo pasar (
		int x = Integer.parseInt(in.nextToken());
		in.nextToken();//Dejo pasar ,
		int y = Integer.parseInt(in.nextToken());
		in.nextToken();//leo el )
		return new Posicion(x, y);
    }
}
