package backend;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Personaje {
	
	private String nombre;
	private ArrayList<Habilidad> habilidades;
	private int vidaMaxima;
	private int vidaActual;
	private int velocidad;
	private ArrayList<String> victimas;
	
	public Personaje(ArrayList<Habilidad> lasHabilidades, String elNombre, int laVida, int laVelocidad){
		this.nombre = elNombre;
		this.vidaMaxima = laVida;
		this.vidaActual = laVida;
		this.velocidad = laVelocidad;
		this.habilidades = lasHabilidades;
		this.victimas = new ArrayList<String>();
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public ArrayList<Habilidad> getHabilidades() {
		return this.habilidades;
	}
	
	public int getVidaMaxima(){
		return this.vidaMaxima;
	}
	
	public int getVidaActual(){
		return this.vidaActual;
	}
	
	public void	setVidaActual(int vida){
		this.vidaActual = vida;
	}

	public int getVelocidad(){
		return this.velocidad;
	}
	
	public ArrayList<String> getVictimas(){
		return this.victimas;
	}
	
	public void agregarVictima(String victima){
		this.victimas.add(victima);
	}
	
    public static void escribir(Writer out, Personaje per) throws IOException{
    	out.write(per.getNombre()+" "+ per.getVidaMaxima()+" "+ per.getVidaActual()+" "+per.getVelocidad()+" ");
    	
    	ArrayList<Habilidad> habilidades = per.getHabilidades();
    	out.write("[ ");
    	Habilidad.escribir(out, habilidades.get(0));
    	out.write(" ");
    	Habilidad.escribir(out, habilidades.get(1));
    	out.write(" ] ");

    	ArrayList<String> victimas = per.getVictimas();
    	out.write("[ ");
    	int i = 0;
	    while(i < victimas.size()-1){
	    	out.write(victimas.get(i)+" , ");
	    	i++;
	    }
	    if (victimas.size() > 0){
	    	out.write(victimas.get(i));
	    }
	    out.write(" ]");
	}
    
	public static Personaje leer(StringTokenizer in) throws IOException{
		String nombre = in.nextToken();
		int vidaMaxima = Integer.parseInt(in.nextToken());
		int vidaActual = Integer.parseInt(in.nextToken());
		int velocidad = Integer.parseInt(in.nextToken());
		
		in.nextToken(); //Dejo pasar el corchete

		ArrayList<Habilidad> habilidades = new ArrayList<Habilidad>();
		StringTokenizer habilidadALeer = new StringTokenizer(in.nextToken()+" "+in.nextToken());
		Habilidad h1 = Habilidad.leer(habilidadALeer);
		habilidadALeer = new StringTokenizer(in.nextToken()+" "+in.nextToken());
		Habilidad h2 = Habilidad.leer(habilidadALeer);
		habilidades.add(h1);
		habilidades.add(h2);
		
		in.nextToken();//Dejo pasar ]
		in.nextToken();//Dejo pasar [
		
		Personaje personaje = new Personaje(habilidades, nombre, vidaMaxima, velocidad);
		personaje.setVidaActual(vidaActual);
		
		String token = in.nextToken();
		while (!token.equals("]")){
			if (token.equals(",")){
				token = in.nextToken();
			}
			personaje.agregarVictima(token);
			token = in.nextToken();
		}
		
		return personaje;
    }
}
