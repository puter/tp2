package backend;

import java.io.IOException;
import java.io.Writer;
import java.util.StringTokenizer;

public class Habilidad 
{

	private TipoHabilidad tipo;
	private int level;

	public Habilidad(TipoHabilidad elTipo){
		this.tipo = elTipo;
		this.level = 1;
	}
	
	public TipoHabilidad getTipo(){
		return this.tipo;
	}
	
	public int getRango() {
		int rango = 0;
		if(this.tipo == TipoHabilidad.Sanar){
			rango = this.getLevel() * 3;
		}else if(this.tipo == TipoHabilidad.Atacar){
			rango = this.getLevel() * 2;			
		}else if(this.tipo == TipoHabilidad.AtacarLejos){
			rango = this.getLevel() * 3;			
		}else if(this.tipo == TipoHabilidad.AtacarFuerte){
			rango = 1;			
		}else if(this.tipo == TipoHabilidad.ChuckNorris){
			if(this.getLevel() <= 100){
				rango = 1;
			}else{
				rango = 99999;
			}			
		}
		return rango;
	}
	
	public int getPoder() {
		int poder = 0;
		if(this.tipo == TipoHabilidad.Sanar){
			poder = this.getLevel() * 5;
		}else if(this.tipo == TipoHabilidad.Atacar){
			poder = this.getLevel() * 3;			
		}else if(this.tipo == TipoHabilidad.AtacarLejos){
			poder = this.getLevel() * 2;			
		}else if(this.tipo == TipoHabilidad.AtacarFuerte){
			poder = this.getLevel() * 5;						
		}else if(this.tipo == TipoHabilidad.ChuckNorris){
			if(this.getLevel() <= 100){
				poder = 1;
			}else{
				poder = 99999;
			}			
		}
		return poder;
	}

	public int getLevel(){
		return this.level;	
	}
	
	public void setLevel(int level){
		this.level = level;
	}
    
    public static void escribir(Writer out, Habilidad t) throws IOException{
    	out.write(t.tipo.name()+" "+ t.level);
	}
	
	public static Habilidad leer(StringTokenizer in) throws IOException{
		TipoHabilidad tipo = TipoHabilidad.valueOf(in.nextToken());
		int level = Integer.parseInt(in.nextToken());
		Habilidad habilidad = new Habilidad(tipo);
		habilidad.setLevel(level);
		return habilidad;
    }
}
