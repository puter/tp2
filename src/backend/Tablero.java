package backend;

import java.io.IOException;
import java.io.Writer;
import java.util.StringTokenizer;

public class Tablero 
{
	private int ancho;
	private int alto;
	
	public Tablero(int an, int al)
	{
		ancho = an;
		alto = al;
	}
	
	public int getAncho() {
		return ancho;
	}
	
	public int getAlto() {
		return alto;
	}
	
	public static void escribir(Writer out, Tablero t) throws IOException{
    	out.write("( "+t.getAncho()+" , "+t.getAlto()+" )");
	}
	
	public static Tablero leer(StringTokenizer in) throws IOException{
		in.nextToken();//Dejo pasar (
		int ancho = Integer.parseInt(in.nextToken());
		in.nextToken();//Dejo pasar ,
		int alto = Integer.parseInt(in.nextToken());
		in.nextToken();//leo el )
		return new Tablero(ancho, alto);
    }

}
