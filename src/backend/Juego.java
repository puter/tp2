package backend;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Juego {

	private Tablero tablero;
	private ArrayList<Personaje> personajes;
	private Map<String, Posicion> posiciones;

	public Juego(ArrayList<Personaje> p, Map<String, Posicion> pos, Tablero t) {
		this.tablero = t;
		this.personajes = p;
		this.posiciones = pos;
	}

	public Tablero getTablero() {
		return this.tablero;
	}

	public ArrayList<Personaje> getPersonajes() {
		return this.personajes;
	}

	public Posicion posicion(Personaje p) {
		return this.posiciones.get(p.getNombre());
	}

	public boolean jaqueMate(Personaje p) {
		int i = 0;
		boolean loPuedenMatar = false;
		ArrayList<Personaje> vivos = perVivos();// solo lo pueden matar si estan vivos
		while (i < vivos.size() && !loPuedenMatar) {
			Personaje player = vivos.get(i);
			if (!player.getNombre().equals(p.getNombre())) {
				int j = 0;
				while (j < player.getHabilidades().size() && !loPuedenMatar) {
					Habilidad h = player.getHabilidades().get(j);
					if (h.getTipo() != TipoHabilidad.Sanar) {
						if (h.getRango() >= distancia(p, player) && h.getPoder() >= p.getVidaActual()) {
							loPuedenMatar = true;
						}
					}
					j++;
				}
			}
			i++;
		}
		return loPuedenMatar;
	}

	public ArrayList<Personaje> victimasPorCercania(Personaje p) {
		// requiere p en juego y vidaActual(p)>0, se puede atacar a el mismo
		ArrayList<Personaje> vivos = perVivos();
		ArrayList<Personaje> victimasPCercania = new ArrayList<Personaje>();
		int i = 0;
		while (i < vivos.size()) {
			Personaje personaje2 = vivos.get(i);
			int dist = distancia(p, personaje2);
			ArrayList<Habilidad> habilidadesDep = p.getHabilidades();
			Habilidad h1 = habilidadesDep.get(0);
			Habilidad h2 = habilidadesDep.get(1);
			if (h1.getRango() >= dist || h2.getRango() >= dist) {
				int j = 0;
				while (j < victimasPCercania.size() && dist > distancia(victimasPCercania.get(j), p)) {
					j++;
				}
				victimasPCercania.add(j, personaje2);
			}
			i++;
		}
		return victimasPCercania;
	}

	private ArrayList<Personaje> perVivos() {
		ArrayList<Personaje> vivos = new ArrayList<Personaje>();
		int i = 0;
		while (i < personajes.size()) {
			Personaje p = personajes.get(i);
			if (p.getVidaActual() > 0) {
				vivos.add(p);
			}
			i++;
		}
		return vivos;
	}

	public ArrayList<Posicion> posicionesSeguras(Personaje p) {
		ArrayList<Posicion> ret = new ArrayList<Posicion>();
		ArrayList<Personaje> vivos = perVivos();
		int x = 0;
		while (x < tablero.getAncho()) {
			int y = 0;
			while (y < tablero.getAlto()) {
				boolean esSegura = true;
				int distanciaAlPersonaje = Math.abs(x - posicion(p).getX()) + Math.abs(y - posicion(p).getY());
				if (distanciaAlPersonaje <= p.getVelocidad()) {
					int i = 0;
					while (i < vivos.size() && esSegura) {
						Personaje player = vivos.get(i);
						if (!player.getNombre().equals(p.getNombre())) {
							int distAlEnemigo =
									Math.abs(x - posicion(player).getX()) + Math.abs(y - posicion(player).getY());
							int h = 0;
							while (h < player.getHabilidades().size() && esSegura) {
								int rango = player.getHabilidades().get(h).getRango();
								if (rango >= distAlEnemigo) esSegura = false;
								h++;
							}
						}
						i++;
					}
					if (esSegura) ret.add(new Posicion(x, y));
				}
				y++;
			}
			x++;
		}
		return ret;

	}

	public ArrayList<Personaje> losMasPoderosos() {
		ArrayList<Personaje> poderosos = new ArrayList<Personaje>();
		if (personajes.size() > 0) {
			poderosos.add(personajes.get(0));
			int i = 1;
			int sumaMayor = sumarHabilidades(poderosos.get(0));
			while (i < personajes.size()) {
				int sumaActual = sumarHabilidades(personajes.get(i));
				if (sumaActual > sumaMayor) {
					poderosos.clear();
					poderosos.add(personajes.get(i));
					sumaMayor = sumaActual;
				} else if (sumaActual == sumaMayor) {
					poderosos.add(personajes.get(i));
				}
				i++;
			}
		}
		return poderosos;
	}

	private int sumarHabilidades(Personaje p) {
		int suma = 0;
		int i = 0;
		while (i < p.getHabilidades().size()) {
			suma += p.getHabilidades().get(i).getLevel();
			i++;
		}
		return suma;
	}

	public Personaje elVengador() {
		// requiere PlayersValido : |players(j)| > 0;
		Personaje elVengador = getPersonajes().get(0);
		int i = 1;
		while (i < getPersonajes().size()) {
			if (venganzas(getPersonajes().get(i)) > venganzas(elVengador)) {
				elVengador = getPersonajes().get(i);
			}
			i++;
		}
		return elVengador;
	}

	private int venganzas(Personaje p) {
		int venganzas = 0;
		int i = 0;
		while (i < p.getVictimas().size()) {
			venganzas += personajePorNombre(p.getVictimas().get(i)).getVictimas().size();
			i++;
		}
		return venganzas;
	}

	private Personaje personajePorNombre(String nombre) {
		Personaje personaje = null;
		int i = 0;
		while (i < this.getPersonajes().size()) {
			if (this.getPersonajes().get(i).getNombre().equals(nombre)) {
				personaje = this.getPersonajes().get(i);
			}
			i++;
		}
		return personaje;
	}

	public void teletransportacion() {
		ArrayList<Personaje> vivos = perVivos();
		Personaje minVida = vivos.get(0);
		Personaje maxVida = vivos.get(0);
		int i = 0;
		while (i < vivos.size()) {
			Personaje otro = vivos.get(i);
			if (otro.getVidaActual() > maxVida.getVidaActual()) {
				maxVida = otro;
			}
			if (otro.getVidaActual() < minVida.getVidaActual()) {
				minVida = otro;
			}
			i++;
		}
		int vida = maxVida.getVidaActual();
		Posicion pos = posicion(maxVida);
		maxVida.setVidaActual(Math.min(minVida.getVidaActual(), maxVida.getVidaMaxima()));
		posiciones.put(maxVida.getNombre(), posicion(minVida));
		minVida.setVidaActual(Math.min(vida, minVida.getVidaMaxima()));
		posiciones.put(minVida.getNombre(), pos);
	}

	// ---atacar---

	public void atacar(Personaje p) {
		Personaje personajeAAtacar;
		Habilidad habilidadParaAtacar;
		ArrayList<Personaje> victimasPorCercania = victimasPorCercania(p);
		victimasPorCercania.remove(personajePorNombre(p.getNombre()));
		if (victimasPorCercania.size() > 0) {
			habilidadParaAtacar = p.getHabilidades().get(0);
			if (habilidadParaAtacar.getTipo() == TipoHabilidad.Sanar
					|| p.getHabilidades().get(1).getPoder() > habilidadParaAtacar.getPoder()) {
				habilidadParaAtacar = p.getHabilidades().get(1);
			}
			ArrayList<Personaje> jugadoresEnJaqueMate = jugadoresEnJaqueMate(habilidadParaAtacar, victimasPorCercania, p);
			if (jugadoresEnJaqueMate.size() > 0) {
				personajeAAtacar = jugadoresEnJaqueMate.get(0);
			} else {
				personajeAAtacar = victimasPorCercania.get(0);
			}
			atacar(p, personajeAAtacar, habilidadParaAtacar);
		}
	}

	private ArrayList<Personaje> jugadoresEnJaqueMate(Habilidad h, ArrayList<Personaje> atacables, Personaje atacante) {
		// requiere que todo los atacables esten en el rango del personaje al cual la habilidad le pertenece
		// requiere tipo(h) != TipoHabilidad.Sanar
		int i = 0;
		ArrayList<Personaje> result = new ArrayList<Personaje>();
		while (i < atacables.size()) {
			if (h.getRango() >= distancia(atacante, atacables.get(i)) && h.getPoder() >= atacables.get(i).getVidaActual()) {
				result.add(atacables.get(i));
			}
			i++;
		}
		return result;
	}

	private void atacar(Personaje atacante, Personaje atacado, Habilidad h) {
		// requiere que el personajeAAtacar este en el rango del personaje al cual la habilidad le pertenece
		// requiere tipo(h) != TipoHabilidad.Sanar
		
		atacado.setVidaActual(atacado.getVidaActual() - h.getPoder());
		
		if (atacado.getVidaActual() <= 0) {
			atacado.setVidaActual(0);
			h.setLevel(h.getLevel() + 1);
			this.posiciones.remove(atacado.getNombre());
			atacante.agregarVictima(atacado.getNombre());
		} else if (atacado.getVidaActual() > atacado.getVidaMaxima()) {
			atacado.setVidaActual(atacado.getVidaMaxima());
		}
	}

	// ---atacar---

	public static void escribir(Writer out, Juego t) throws IOException {
		// Estructura de impresion: Tablero [nombre(personaje(vivo)) Posicion] [Personaje(vivosymuertos)]
		Tablero.escribir(out, t.getTablero());

		out.write(" [ ");
		ArrayList<Personaje> vivos = t.perVivos();
		int i = 0;
		while (i < vivos.size()) {
			out.write(vivos.get(i).getNombre() + " ");
			Posicion.escribir(out, t.posicion(vivos.get(i)));
			if (i < vivos.size() - 1) {
				out.write(" , ");
			}
			i++;
		}

		out.write(" ] ");

		out.write("[ ");
		i = 0;
		while (i < t.getPersonajes().size()) {
			Personaje.escribir(out, t.getPersonajes().get(i));
			if (i < t.getPersonajes().size() - 1) {
				out.write(" , ");
			}
			i++;
		}

		out.write(" ] ");
	}

	public static Juego leer(StringTokenizer in) throws IOException {

		Tablero tablero = Tablero.leer(in);

		in.nextToken();// Dejo pasar "["

		Map<String, Posicion> posiciones = new HashMap<String, Posicion>();
		String token = in.nextToken();
		while (!token.equals("]")) {
			if (token.equals(",")) {
				token = in.nextToken();
			}
			String nombrePersonaje = token;
			Posicion posicionPersonaje = Posicion.leer(in);
			posiciones.put(nombrePersonaje, posicionPersonaje);
			token = in.nextToken();
		}

		ArrayList<Personaje> personajes = new ArrayList<Personaje>();
		token = in.nextToken();// =="["
		while (!token.equals("]")) {
			try {
				personajes.add(Personaje.leer(in));
				token = in.nextToken();
			} catch (IOException e) {
				System.out.println("No hay jugadores");// Aca entra cuando el array de jugadores viene vacio
				token = "]";
			}
		}
		
		//Elimino posibles posiciones de personajes muertos
		int i = 0;
		while (i < personajes.size()){
			Personaje personajeActual = personajes.get(i);
			if (personajeActual.getVidaActual() == 0){
				posiciones.remove(personajeActual.getNombre());
			}
			i++;
		}

		return new Juego(personajes, posiciones, tablero);
	}
	
	public Personaje asesinosSeriales() {
		return this.asesinosSerialesLista().get(0);
	}
	
	public ArrayList<Personaje> asesinosSerialesLista() {
		int masvictimas=0;
		int i=0;
		while(i < getPersonajes().size()){
			int victimas = getPersonajes().get(i).getVictimas().size();
			if(masvictimas < victimas){
				masvictimas = victimas;
			}
			i++;
		}
		ArrayList<Personaje> result=new ArrayList<Personaje>();
		i=0;
		while(i<getPersonajes().size()){
			if(getPersonajes().get(i).getVictimas().size()==masvictimas){
				result.add(getPersonajes().get(i));
			}
			i++;
		}
		return result;
	}
	
	public void muerteSubita(){
		int maxdistancia=0;
		int i=0;
		ArrayList<Personaje> personajes=perVivos();
		while(i<personajes.size()){
			int distancia=distanciaalcentro(personajes.get(i));
			if(maxdistancia<distancia){
				maxdistancia=distancia;
			}
			i++;
		}
		i=0;
		while(i<personajes.size()){
			Personaje personaje=personajes.get(i);
			if(!esChuck(personaje)){
				if(distanciaalcentro(personaje)==maxdistancia){
					personaje.setVidaActual(0);
					posiciones.remove(personaje.getNombre());
				}else{
					personaje.setVidaActual(1);
				}
			}
			i++;
		}
	}
	
	private boolean esChuck(Personaje p1){
		return ((p1.getHabilidades().get(0).getTipo()==TipoHabilidad.ChuckNorris && p1.getHabilidades().get(0).getLevel()>100)
					|| (p1.getHabilidades().get(1).getTipo()==TipoHabilidad.ChuckNorris && p1.getHabilidades().get(1).getLevel()>100));
	}
	
	//Habria que ver como definimos el centro
	public int distanciaalcentro(Personaje p1) {
		return Math.abs(posiciones.get(p1.getNombre()).getX() - (tablero.getAlto()/2))
				+ Math.abs(posiciones.get(p1.getNombre()).getY() - (tablero.getAncho()/2));
	}

	private int distancia(Personaje p1, Personaje p2) {
		return Math.abs(posiciones.get(p1.getNombre()).getX() - posiciones.get(p2.getNombre()).getX())
				+ Math.abs(posiciones.get(p1.getNombre()).getY() - posiciones.get(p2.getNombre()).getY());
	}

}
